import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  pizzes: pizzaElement[] = [
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/06e9cb81-7b39-4cb0-9856-fbe0782baea6.jpg",title: 'Пепперони', text: 'Томатный соус, пикантная пепперони, моцарелла', price: '9.90'},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/7bab478f-79a6-4ce4-9c5e-c21e6a3909e6.jpg",title: 'Сырный цыпленок', text: 'Сырный соус, цыпленок, томаты, моцарелла', price: '14.40'},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/cf4d9ad5-5979-4acb-b00f-c2b2d2fd92b9.jpg",title: 'Ветчина и грибы', text: 'Томатный соус, моцарелла, ветчина, шампиньоны', price: '6.90'},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/6233cc6d-b7a4-4703-8b3f-5712c206b8c5.jpg",title: 'Супермясная', text: 'Острая чоризо, митболы из говядины, бекон', price: "18.90"},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/b2c53e81-c2a0-4e37-83e4-ccaa0c434255.jpg",title: 'Мясная', text: 'Острая чоризо, томатный соус, цыпленок', price: "16.90"},
    {image: "https://cdn.dodostatic.net/static/Img/Products/ed5451b63cc742f8a94d0db6eb398893_233x233.jpeg",title: 'Дон Бекон', text: 'Томатный соус, цыпленок филе, пикантная пепперони', price: "15.60"},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/297f17fa-c23b-492a-934c-ccc2233fde5c.jpg",title: 'Цыпленок барбекю', text: 'Соус барбекю, томатный соус, цыпленок', price: "13.30"},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/f7e14d01-c675-4a17-b383-fcd577350b5a.jpg",title: 'Овощи и грибы', text: 'Итальянские травы, томатный соус, томаты, кубики брынзы, маслины, красный лук', price: "9.90"}
  ]

  constructor() { }

  ngOnInit(): void {
  }

}

interface pizzaElement {
  image: string,
  title: string,
  text: string,
  price: string
}
