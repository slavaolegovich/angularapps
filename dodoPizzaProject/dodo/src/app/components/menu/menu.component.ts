import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  pizzesFirstRow: pizzaElement[] = [
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/06e9cb81-7b39-4cb0-9856-fbe0782baea6.jpg",title: 'Пепперони', text: 'Томатный соус, пикантная пепперони, моцарелла', price: '9.90'},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/7bab478f-79a6-4ce4-9c5e-c21e6a3909e6.jpg",title: 'Сырный цыпленок', text: 'Сырный соус, цыпленок, томаты, моцарелла', price: '14.40'},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/cf4d9ad5-5979-4acb-b00f-c2b2d2fd92b9.jpg",title: 'Ветчина и грибы', text: 'Томатный соус, моцарелла, ветчина, шампиньоны', price: '6.90'},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/6233cc6d-b7a4-4703-8b3f-5712c206b8c5.jpg",title: 'Супермясная', text: 'Острая чоризо, митболы из говядины, бекон', price: "18.90"}
  ]

  pizzesSecondRow: pizzaElement[] = [
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/b2c53e81-c2a0-4e37-83e4-ccaa0c434255.jpg",title: 'Мясная', text: 'Острая чоризо, томатный соус, цыпленок', price: "16.90"},
    {image: "https://cdn.dodostatic.net/static/Img/Products/ed5451b63cc742f8a94d0db6eb398893_233x233.jpeg",title: 'Дон Бекон', text: 'Томатный соус, цыпленок филе, пикантная пепперони', price: "15.60"},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/297f17fa-c23b-492a-934c-ccc2233fde5c.jpg",title: 'Цыпленок барбекю', text: 'Соус барбекю, томатный соус, цыпленок', price: "13.30"},
    {image: "https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/f7e14d01-c675-4a17-b383-fcd577350b5a.jpg",title: 'Овощи и грибы', text: 'Итальянские травы, томатный соус, томаты, кубики брынзы, маслины, красный лук', price: "9.90"}
  ]

  mytest: number = 20.3;

  title: string;
  image: string;
  text: string;
  price: string;
  minPrice: string;

  height: string = '150px';
  width: string = '150px';

  constructor() { }

  ngOnInit(): void {
  }

  changeInfo (item: pizzaElement): void {
    this.title = item.title;
    this.text = item.text;
    this.image = item.image;
    this.price = item.price;
    this.minPrice = item.price;
  }

  test () {
    console.log("hello");
  }

  changeImageF (price: string) {
    this.height = '150px';
    this.width = '150px';
    this.price = this.minPrice;
  }

  changeImageS (price: string) {
    this.height = '160px';
    this.width = '160px';
    this.price = String(Number(this.minPrice) + 2) + '0';
  }

  changeImageT (price: string) {
    this.height = '170px';
    this.width = '170px';
    this.price = String(Number(this.minPrice) + 4) + '0';
  }

}

interface pizzaElement {
  image: string,
  title: string,
  text: string,
  price: string
}
