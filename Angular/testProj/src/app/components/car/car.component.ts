import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  name: string;
  model: string;
  speed: number;
  colors: Colors;
  options: string[];
  isEdit: boolean = false;

  constructor() { }

  ngOnInit(): void {
    this.name = 'Audi';
    this.model = 'RS8';
    this.speed = 200;
    this.colors = {
      car: "white",
      salon: "black",
      wheels: 'silver'
    };
    this.options = ['ABS', "Autopilot", "Parking"]
  }

  showEdit() {
    this.isEdit = !this.isEdit;
  }

  addOpt (option) {
    this.options.unshift(option); // Записать элемент в начало массива 
    return false;
  }

  deleteOpt (option) {
    for (let i = 0; i < this.options.length; i++) {
      if (this.options[i] == option) {
        this.options.splice(i, 1);
        break;
      }
    }
  }

  carSelect (name) {
    if (name == "BMW") {
      this.name = 'BMW';
      this.model = 'X8';
      this.speed = 230;
      this.colors = {
        car: "lack",
        salon: "black",
        wheels: 'silver'
      };
      this.options = ['ABS', "Autopilot", "Parking"]
    } else if (name == "Audi") {
      this.name = 'Audi';
      this.model = 'RS8';
      this.speed = 200;
      this.colors = {
        car: "white",
        salon: "black",
        wheels: 'silver'
      };
      this.options = ['TES', "Autopilot", "Parking"]
    } else {
      this.name = 'Mercedes';
      this.model = 'AMG';
      this.speed = 300;
      this.colors = {
        car: "white",
        salon: "black",
        wheels: 'lack'
      };
      this.options = ['ABS', "Autopilot", "Parking"]
    }
  }
}

interface Colors {
    car: string,
    salon: string, 
    wheels: string
}